# SimpleHTTPServerWithUpload

Taken from https://gist.github.com/smidgedy/1986e52bb33af829383eb858cb38775c

Added upload.html

How to use:

0) edit upload.html to point to the IP address of your local machine

1) creates a HTTP server in
```
python3 -m http.server
```
2) start the upload listener on port 4444
```
python3 SimpleHTTPServerWithUpload.py
```
3) browse to the HTTP server

```
http://<machine_ip>:8000
```

4.1) See the directory and download any file you want, or...

4.2) click on upload.html and then rock&roll
